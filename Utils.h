#pragma once

#include <string>
#include <map>
#include <stdexcept>

#include "Properties.h"

using namespace std;

class TString : public string
{
public:
  //using std::string::string;
  TString() {}
  TString(string String)
  {
    this->assign(String);
  }
  TString(const char *String)
  {
    this->assign(String);
  }
  int ToInt()
  {
    int Result;
    try
    {
      Result = stoi(*this);
    }
    catch(...)
    {
      return -1;
    }
    return Result;
  }
  bool ToInt(int &Value)
  {
    try
    {
      Value = stoi(*this);
    }
    catch(...)
    {
      return false;
    }
    return true;
  }
};

class TStringList
{
private:
  map<string, string> FItems;
  void Init()
  {
    Text.Init(this, &TStringList::GetText, &TStringList::SetText);
    Values.Init(this, &TStringList::GetItem, &TStringList::SetItem);
  }
  string GetText()
  {
    string Result;
    for(auto i = FItems.begin(); i != FItems.end(); i++)
      if(!i->second.empty())
      {
        Result += i->first + '=';
        Result += i->second + '\n';
      }
      else
        Result += i->first + '\n';
    return Result;
  }
  void SetText(string Text)
  {
    stringstream ss;
    ss.str(Text);
    string Item;
    while (getline(ss, Item, '\n'))
    {
      size_t i = Item.find('=');
      if(i == string::npos)
        FItems[Item] = "";
      else
        FItems[Item.substr(0, i)] = Item.substr(i + 1);
    }
  }
  TString GetItem(string Key)
  {
    auto i = FItems.find(Key);
    return i == FItems.end() ? "" : TString(i->second);
  }
  void SetItem(string Key, TString Value)
  {
    FItems[Key] = Value;
  }
public:
  TStringList()
  {
    Init();
  }
  TStringList(string Text)
  {
    Init();
    this->Text = Text;
  }
  void Add(string String)
  {
    FItems[String];
  }
  void Clear()
  {
    FItems.clear();
  }
  TProperty <TStringList, string> Text;
  TIndexedProperty<TStringList, string, TString> Values;
};
