#include "pch.h"

using namespace std;


//WinApi
#include <cstring>
#include <stdlib.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <vector>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <sstream>

char http[]= {"http://"};
char host[]= {"localhost"};
char parm[]= {"/api/method.php"};


std::string getProtocol(std::string url)
{
  std::string protocol = "";

  int i = 0;

  for(i = 0; i < url.size(); i++)
  {
    if(url[i] != '/' || url[i+1] != '/')
    {
      protocol += url[i];
    }
    else
    {
      protocol += "//";
      break;
    }
  }

  return protocol;
}

std::string getHost(std::string url)
{
  std::string host = "";

  url.replace(0, getProtocol(url).size(), "");

  int i = 0;
  for(i = 0; i < url.size(); i++)
  {

    if(url[i] != '/')
    {
      host += url[i];
    }
    else
    {
      break;
    }

  }

  return host;
}

std::string getAction(std::string url)
{
  std::string parm = "";

  url.replace(0, getProtocol(url).size()+getHost(url).size(), "");

  int i = 0;
  for(i = 0; i < url.size(); i++)
  {

    if(url[i] != '?' && url[i] != '#')
    {
      parm += url[i];
    }
    else
    {
      break;
    }

  }

  return parm;
}

std::string getParams(std::vector< std::pair< std::string, std::string> > requestData)
{
  std::string parm = "";

  std::vector< std::pair< std::string, std::string> >::iterator itr = requestData.begin();

  for(; itr != requestData.end(); ++itr)
  {
    if(parm.size() < 1)
    {
      parm += "";
    }
    else
    {
      parm += "&";
    }
    parm += itr->first + "=" + itr->second;
  }

  return parm;
}


std::string GET(std::string url, std::vector< std::pair< std::string, std::string> > requestData)
{
  std::string http = getProtocol(url);
  std::string host = getHost(url);
  std::string script = getAction(url);
  std::string parm = getParams(requestData);

  char buf[1024];

  std::string header = "";

  header += "GET ";
  header += http + host + script + "?" + parm;
  header += (std::string)" HTTP/1.1" + "\r\n";
  header += (std::string)"Host: " + http + host + "/" + "\r\n";
  header += (std::string)"User-Agent: Mozilla/5.0" + "\r\n";
  //header += (std::string)"Accept: text/html" + "\r\n";
  header += (std::string)"Accept-Language: ru,en-us;q=0.7,en;q=0.3" + "\r\n";
  header += (std::string)"Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7" + "\r\n";
  header += (std::string)"Connection: keep-alive " + "\r\n";
  header += "\r\n";



  int sock;
  struct sockaddr_in addr;
  struct hostent* raw_host;
  raw_host = gethostbyname(host.c_str());
  if(raw_host == NULL)
  {
    std::cout<<"ERROR, no such host";
    exit(0);
  }

  sock = socket(AF_INET, SOCK_STREAM, 0);

  addr.sin_family = AF_INET;
  addr.sin_port = htons(80);

  bcopy((char*)raw_host->h_addr, (char*)&addr.sin_addr, raw_host->h_length);

  if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
  {
    std::cerr<<"connect error"<<std::endl;
    exit(2);
  }


  char * message = new char[ header.size() ];
  for(int i = 0; i < header.size(); i++)
  {
    message[i] = header[i];
  }

  send(sock, message, header.size(), 0);
  recv(sock, buf, sizeof(buf), 0);

  std::string answer = "";

  for(int j = 0; j < 1024; j++)
  {
    answer += buf[j];
  }

  return answer;

}








//atl
//#include <windows.h>
//#include <atlbase.h>
//#include <msxml6.h>

//HRESULT hr;
//CComPtr<IXMLHTTPRequest> request;

//hr = request.CoCreateInstance(CLSID_XMLHTTP60);
//hr = request->open(
//      _bstr_t("GET"),
//      _bstr_t("https://www.google.com/images/srpr/logo11w.png"),
//      _variant_t(VARIANT_FALSE),
//      _variant_t(),
//      _variant_t());
//hr = request->send(_variant_t());
//// get status - 200 if succuss
//long status;
//hr = request->get_status(&status);
//// load image data (if url points to an image)
//VARIANT responseVariant;
//hr = request->get_responseStream(&responseVariant);
//IStream* stream = (IStream*)responseVariant.punkVal;
//CImage image = new CImage();
//image->Load(stream);
//stream->Release();





//pistache asio boost
//#include <pistache/endpoint.h>

//using namespace Net;

//struct HelloHandler : public Http::Handler
//{
//  HTTP_PROTOTYPE(HelloHandler)

//  void onRequest(const Http::Request& request, Http::ResponseWriter writer)
//  {
//    writer.send(Http::Code::Ok, "Hello, World!");
//  }
//};

//int main() {
//  Http::listenAndServe<HelloHandler>("*:9080");
//}




//restbed
//using namespace restbed;
//void print(const shared_ptr< Response >& response)
//{
//  response->get_status_code();
//  fprintf(stderr, "*** Response ***\n");
//  fprintf(stderr, "Status Code:    %i\n", response->get_status_code());
//  fprintf(stderr, "Status Message: %s\n", response->get_status_message().data());
//  fprintf(stderr, "HTTP Version:   %.1f\n", response->get_version());
//  fprintf(stderr, "HTTP Protocol:  %s\n", response->get_protocol().data());

//  for(const auto header : response->get_headers())
//  {
//    fprintf(stderr, "Header '%s' > '%s'\n", header.first.data(), header.second.data());
//  }

//  auto length = response->get_header("Content-Length", 0);

//  Http::fetch(length, response);

//  fprintf(stderr, "Body:           %.*s...\n\n", 25, response->get_body().data());
//}

//int main(const int, const char**)
//{
//  auto request = make_shared< Request >(Uri("http://www.corvusoft.co.uk:80/?query=search%20term"));
//  request->set_header("Accept", "*/*");
//  request->set_header("Host", "www.corvusoft.co.uk");

//  auto response = Http::sync(request);
//  print(response);

//  auto future = Http::async(request, [ ](const shared_ptr<Request>, const shared_ptr<Response> response)
//  {
//    fprintf(stderr, "Printing async response\n");
//    print(response);
//  });

//  future.wait();
//  return EXIT_SUCCESS;
//}

//void Request()
//{
//  UrlRequest request;
//  request.port(6000);
//  request.host("127.0.0.1");
//  const auto countryId = 1;
//  const auto count = 1000;
//  request.uri("/method/database.getCities", {
//                {"lang","ru"},
//                {"country_id",countryId},
//                {"count",count},
//                {"need_all","1"},
//              });
//  request.addHeader("Content-Type: application/json");
//  auto response = std::move(request.perform());
//  if(response.statusCode() == 200)
//    cout << "status code = "<<response.statusCode()<<", body = *"<<response.body()<<"*"<<endl;
//  else
//    cout << "status code = "<<response.statusCode()<<", description = "<<response.statusDescription()<<endl;
//}
