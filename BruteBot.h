#include "stdafx.h"
#include "Utils/Utils.h"
#include "Utils/Socket.h"
#include "Utils/OsName.h"

using namespace std;

class TBruteBot
{
private:
  int BotId;
  bool Update();
public:
  TSocket Socket;
  unique_ptr<TScanner> Scanner;
  bool Testing;
  bool IgnoreUpdate;
  TBruteBot();
  bool AddToServer();
  bool Run();
};